<?php

namespace Drupal\neon_api;

/**
 * Neon APi Interface.
 */
interface NeonApiInterface {

  /**
   * Gets the session id.
   */
  public function getSessionId();

  /**
   * Returns event attendees list.
   */
  public function getEventAttendees($eventId, $pageSize);

  /**
   * Returns individual account info.
   */
  public function getIndividualAccount($accountId);

}
