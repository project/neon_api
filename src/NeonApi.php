<?php

namespace Drupal\neon_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;

/**
 * Neon Api Service.
 */
class NeonApi implements NeonApiInterface {

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * User session id.
   *
   * @var string
   */
  protected $userSessionId;

  /**
   * NeonApiCalls constructor.
   *
   * @param \GuzzleHttp\Client $client
   *   Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The Config.
   */
  public function __construct(Client $client, ConfigFactoryInterface $config) {
    $this->client = $client;
    $this->config = $config;
    $this->userSessionId = $this->getSessionId();
  }

  /**
   * Returns session id.
   */
  public function getSessionId() {
    $config = $this->config->get('neon_api.adminsettings');
    $request = $this->client->get('https://api.neoncrm.com/neonws/services/api/common/login', [
      'query' => [
        'login.apiKey' => $config->get('neon_api_api_key'),
        'login.orgid' => $config->get('neon_api_org_id'),
      ],
    ]);
    $response = Json::decode($request->getBody());
    return $response['loginResponse']['userSessionId'];
  }

  /**
   * Returns event attendees list.
   */
  public function getEventAttendees($eventId, $totalPages) {
    $data = [];
    for ($i = 1; $i <= $totalPages; $i++) {
      $request = $this->client->get('https://api.neoncrm.com/neonws/services/api/event/retrieveEventAttendees', [
        'query' => [
          'userSessionId' => $this->userSessionId,
          'eventId' => $eventId,
          'page.pageSize' => 200,
          'page.currentPage' => $i,
        ],
      ]);
      $response = Json::decode($request->getBody());
      $data = array_merge($response['retrieveEventAttendees']['eventAttendeesResults']['eventAttendeesResult'], $data);
    }
    return $data;
  }

  /**
   * Returns individual account info.
   */
  public function getIndividualAccount($accountId) {
    $request = $this->client->get('https://api.neoncrm.com/neonws/services/api/account/retrieveIndividualAccount', [
      'query' => [
        'userSessionId' => $this->userSessionId,
        'accountId' => $accountId,
      ],
    ]);
    $response = Json::decode($request->getBody());
    return $response['retrieveIndividualAccountResponse']['individualAccount'];
  }

  /**
   * Returns list of custom fields.
   */
  public function getCustomFieldList() {
    $request = $this->client->get('https://api.neoncrm.com/neonws/services/api/common/listCustomFields', [
      'query' => [
        'userSessionId' => $this->userSessionId,
        'searchCriteria.component' => 'Event',
      ],
    ]);
    $response = Json::decode($request->getBody());
    return $response['listCustomFieldsResponse']['customFields']['customField'];
  }

}

